function elapsed(created) {
    var current = Date.now();
    var elapsed = current - created;

    var divideBy = {
        w: 604800000,
        d: 86400000,
        h: 3600000,
        m: 60000,
        s: 1000
    };

    if (elapsed / divideBy['w'] >= 1) {
        return Math.floor(elapsed / divideBy['d']) + (Math.floor(elapsed / divideBy['w']) > 1 ? " weeks ago" : " week ago");
    } else if (elapsed / divideBy['d'] >= 1) {
        return Math.floor(elapsed / divideBy['d']) + (Math.floor(elapsed / divideBy['d']) > 1 ? " days ago" : " day ago");
    } else if (elapsed / divideBy['h'] >= 1) {
        return Math.floor(elapsed / divideBy['h']) + (Math.floor(elapsed / divideBy['h']) > 1 ? " hours ago" : " hour ago");
    } else if (elapsed / divideBy['m'] >= 1) {
        return Math.floor(elapsed / divideBy['m']) + (Math.floor(elapsed / divideBy['m']) > 1 ? " minutes ago" : " minute ago");
    } else {
        return Math.floor(elapsed / divideBy['s']) + (Math.floor(elapsed / divideBy['s']) > 1 ? " seconds ago" : " second ago");
    }

}

module.exports = elapsed;