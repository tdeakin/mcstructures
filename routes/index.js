var express = require('express');
var router = express.Router();
var siteTitle = "Minecraft Structure Database";
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://localhost:27017/mcstructures';
var async = require("async");

var functions = require('../models/functions');

/* GET home page. */
router.get('/', function (req, res, next) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log(err);
        } else {
            console.log("connected to", url);

            var collection = db.collection('structures');

            async.parallel([
                    function (callback) {
                        var cursor = collection.find().limit(20);
                        var recent = [];
                        cursor.each(function (err, doc) {
                            if (doc != null) {
                                recent.push(doc);
                            }
                        });

                        callback(null, recent);
                    },
                    function (callback) {
                        var featured = [];
                        var cursor = collection.find({}).sort({"votes": -1}).limit(4);
                        cursor.each(function (err, doc) {
                            if (doc != null) {
                                featured.push(doc);
                            }
                        });
                        callback(null, featured);
                    },
                    function (callback) {
                        collection.count({})
                            .then(function (data) {
                                callback(null, data);
                            });

                    },
                    function (callback) {
                        db.collection('users').count({})
                            .then(function (data) {
                                callback(null, data);
                            })
                    }
                ],
                function (err, results) {
                    if (err) {
                        console.log(err);
                    }

                    if (results == null || results[0] == null) {
                        console.log("Empty?");
                        console.log(results);
                    }
                    var stats = [
                        {
                            "value": results[2],
                            "text": " Structures in the Database"
                        },
                        {
                            "value": results[3],
                            "text": "Registered users"
                        }
                    ];
                    res.render('index', {
                        siteTitle: siteTitle,
                        username: getUsername(req),
                        recentPosts: results[0],
                        featured: results[1],
                        stats: stats

                    });


                });
        }
    });
});
router.get('/about', function (req, res, next) {
    res.render('page', {
        siteTitle: siteTitle,
        pageTitle: "About",
        username: getUsername(req),
        content: "Minecraft Structure Database is a site for doing stuff"
    })
});


router.get('/structure/:name', function (req, res, next) {
    // get the name of the structure
    var name = req.params.name;
    var hasVoted = false;
    var userid = getUserid(req);
    // connect to DB
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log(err);
        } else {
            console.log("connected to ", url);
            var collection = db.collection('structures');
            // set up initial query
            var query = {};
            // get build
            query["name"] = name;

            collection.findOne(query, function (err, doc) {
                if (err) {
                    console.log(err)
                } else if (doc) {
                    // get author
                    var author = doc['author'];
                    // number of items
                    if (userid && doc['voters'].indexOf(userid) > -1) {
                        hasVoted = true;
                    }
                    collection.count()
                        // promise is fulfilled
                        .then(function (numItems) {
                            // reset query
                            query = {};
                            query["author"] = author;
                            // find other builds by same author
                            collection.find({
                                    $query: {
                                        "author": author,
                                        "name": {
                                            $ne: doc['name']// make sure the list doesn't contain current build
                                        }
                                    }
                                    ,
                                    $orderby: {
                                        $natural: -1
                                    }
                                }
                            ).limit(3).toArray()// only get three, and put them into an array
                                // once promise is fulfilled
                                .then(function (data) {
                                    // display the page
                                    res.render('structure', {
                                        layout: 'sidebarless',
                                        siteTitle: siteTitle,
                                        username: getUsername(req),
                                        stats: numItems,
                                        name: name,
                                        structureName: doc['title'],
                                        author: doc['author'],
                                        description: doc['content'],
                                        screenshot: doc['screenshot'],
                                        category: doc['category'],
                                        tags: doc['tags'],
                                        structure: doc['structure'],
                                        numVotes: doc['votes'],
                                        created: doc['created'],
                                        userHasVoted: hasVoted,
                                        otherWork: data
                                    });
                                })
                        });
                    //nothing exists, therefore 404 - might attempt to make this more graceful later
                } else {
                    res.status(404).render('error', {
                        status: "404",
                        message: "Structure not Found",
                        extended: "Sorry! The structure you were looking for can't be found."
                    });
                }
            })
        }
    })
});


router.get('/member/:name', function (req, res, next) {
    var name = req.params.name;
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log(err);
        } else {
            console.log("connected to ", url);

            var collection = db.collection('structures');

            async.parallel([
                    function (callback) {
                        var cursor = collection.find().limit(20);
                        var recent = [];
                        cursor.each(function (err, doc) {
                            if (doc != null) {
                                recent.push(doc);
                            }
                        });

                        callback(null, recent);
                    },
                    function (callback) {
                        var featured = [];
                        var cursor = collection.find({}).sort({"votes": -1}).limit(4);
                        cursor.each(function (err, doc) {
                            if (doc != null) {
                                featured.push(doc);
                            }
                        });
                        callback(null, featured);
                    },
                    function (callback) {
                        collection.count({})
                            .then(function (data) {
                                callback(null, data);
                            });

                    },
                    function (callback) {
                        db.collection('users').count({})
                            .then(function (data) {
                                callback(null, data);
                            })
                    },
                    function (callback) {
                        var query = {};
                        query["author"] = name;

                        var structures = [];
                        var cursor = collection.find(query);

                        cursor.each(function (err, doc) {
                            if (doc != null) {
                                structures.push(doc);
                            }
                        });

                        callback(null, structures);

                    }
                ],
                function (err, results) {
                    if (err) {
                        console.log(err);
                    }

                    if (results == null || results[0] == null) {
                        console.log("Empty?");
                        console.log(results);
                    }
                    var stats = [
                        {
                            "value": results[2],
                            "text": " Structures in the Database"
                        },
                        {
                            "value": results[3],
                            "text": "Registered users"
                        }
                    ];
                    res.render('member', {
                        name: name,
                        siteTitle: siteTitle,
                        structures: results[4],
                        featured: results[1],
                        username: getUsername(req),
                        stats: stats
                    })
                });
        }
    });
});

router.get('/search', function (req, res, next) {

    var searchQuery = req.query.query;
    console.log(searchQuery);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log(err);
        } else {
            var collection = db.collection('structures');
            var results = [];

            collection.find({
                "$text": {
                    "$search": searchQuery
                }
            }, {
                title: 1,
                screenshot: 1,
                author: 1,
                name: 1,
                created: 1,
                category: 1,
                _id: 1,
                textScore: {
                    $meta: "textScore"
                }
            }, {
                sort: {
                    textScore: {
                        $meta: "textScore"
                    }
                }
            }).toArray()
                .then(function (data) {
                    results = data;
                    console.log(results);
                    res.render('list', {
                        siteTitle: siteTitle,
                        pageTitle: "Search Results",
                        query: searchQuery,
                        results: results,
                        username: getUsername(req)
                    })
                })

        }
    })
});

router.post('/upvote', function (req, res, next) {
    var structureName = req.body.structure;
    var voter = req.session.user._id;
    console.log(structureName);
    var query = {name: structureName, voters: {'$ne': voter}};
    var update = {'$push': {'voters': voter}, '$inc': {votes: 1}};

    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log(err);
        } else {
            var collection = db.collection('structures');
            collection.update(query, update);
            collection.findOne({"name": structureName}, function (err, doc) {
                var response = {
                    "count": doc['votes']
                };
                response = JSON.stringify(response);
                res.status(200).send(response);
            });
        }
    })

});

router.post('/downvote', function (req, res, next) {
    var structureName = req.body.structure;
    var voter = req.session.user._id;

    var query = {name: structureName, voters: voter};
    var update = {'$pull': {'voters': voter}, '$inc': {votes: -1}};

    console.log("downvoting");

    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log(err);
        } else {
            var collection = db.collection('structures');
            collection.update(query, update);
            collection.findOne({"name": structureName}, function (err, doc) {
                var response = {
                    "count": doc['votes']
                };
                response = JSON.stringify(response);
                res.status(200).send(response);
            });
        }
    });
});

router.get('/contact', function (req, res, next) {

    res.render('contact', {
        siteTitle: siteTitle,
        username: getUsername(req),
        pageTitle: "Contact Us"
    })
});

router.get('/:pagename', function (req, res, next) {
    var pages = ["privacy-policy", "terms-of-use", "faq"];
    var pageTitles = ["Privacy Policy", "Terms of Use", "Frequently Asked Questions"];
    var pagename = req.params.pagename;

    console.log(pagename);

    if (pages.indexOf(pagename) > -1) {
        var pageTitle = pageTitles[pages.indexOf(pagename)];
        console.log("exists");

        var text = require('../pages/' + pagename + '.json');
        res.render('page', {
            username: getUsername(req),
            siteTitle: siteTitle,
            pageTitle: pageTitle,
            text: text['pageContent']
        })
    } else {
        res.render('error')// implement error handling
    }


});


function getUsername(req) {
    var user = req.session.user;
    var username;
    if (user) {
        return user.username;
    } else {
        return null;
    }
}
function getUserid(req) {
    var user = req.session.user;
    if (user) {// if there is a user
        return user._id;
    } else {
        return null;
    }
}

module.exports = router;
