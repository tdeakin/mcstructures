var express = require('express');
var router = express.Router();
var multer = require('multer');
var path = require('path');
var siteTitle = "Minecraft Structure Database";
var sanitizeHtml = require('sanitize-html');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
        cb(null,
            path.basename(file.originalname, path.extname(file.originalname))
            + '-' + Date.now() + '-'
            + path.extname(file.originalname))
    }

});

var upload = multer({storage: storage});
var uploadFields = upload.fields([{name: 'structure', maxCount: 1}, {name: 'screenshot', maxCount: 1}]);

var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://localhost:27017/mcstructures';

var isAuthenticated = function (req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isAuthenticated())
        return next();
    // if the user is not authenticated then redirect him to the login page
    res.redirect('/account/login');
};

module.exports = function (passport) {

    /* GET login page*/
    router.get('/login', function (req, res, next) {
        res.render('login', {
            layout: 'sidebarless',
            siteTitle: siteTitle,
            message: req.flash('message')
        });
    });

    router.post('/login', passport.authenticate('login', {
        successRedirect: '/',
        failureRedirect: '/account/login',
        failureFlash: true
    }));

    /* GET Registration Page */
    router.get('/signup', function (req, res) {
        res.render('register', {
            layout: 'sidebarless',
            siteTitle: siteTitle,
            message: req.flash('message')
        });
    });

    /* Handle Registration POST */
    router.post('/signup', passport.authenticate('signup', {
        successRedirect: '/',
        failureRedirect: '/account/signup',
        failureFlash: true
    }));

    router.get('/dashboard', isAuthenticated, function (req, res, next) {

        if (req.session.message) {
            var message = req.session.message;
            req.session.message = null;
        }

        if (message != null) {
            console.log(message);
        } else {
            message = false;
        }

        var structures = [];
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log(err);
            } else {
                console.log("connected to ", url);

                var collection = db.collection('structures');

                var query = {};
                query["author"] = req.session.user.username;

                collection.find(query).toArray()
                    .then(function (data) {
                        res.render('dashboard', {
                            layout: 'sidebarless',
                            siteTitle: 'Minecraft Structures',
                            username: req.session.user.username,
                            structure: data
                        });
                    })

            }
        })
    });

    router.get('/dashboard/edit', isAuthenticated, function (req, res, next) {
        res.render('profile-edit', {
            layout: 'sidebarless',
            username: req.session.user.username
        })
    });

    router.get('/add/', isAuthenticated, function (req, res, next) {

        if (req.session.message) {
            var message = req.session.message;
        }

        res.render('editor', {
            layout: 'sidebarless',
            username: req.session.user.username
        });


    });


    router.post('/submit/structure', isAuthenticated, uploadFields, function (req, res, next) {
        var title = req.body.title;
        title = sanitizeHtml(title, {allowedTags: []});
        var count = 0;
        var name = title.replace(/\s/g, "-").toLowerCase();
        name = sanitizeHtml(name, {allowedTags: []});

        var category = req.body.category;
        var content = req.body.content;
        console.log(content);
        content = sanitizeHtml(content);
        var tags = req.body.tags;
        tags = sanitizeHtml(tags, {allowedTags: []});
        tags = tags.split(",");
        var credit = req.body.credit;
        credit = sanitizeHtml(credit, {allowedTags: []});
        var screenshot = req.files['screenshot'][0].filename;
        var structure = req.files['structure'][0].filename;
        console.log(screenshot);
        console.log(structure);
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log(err)
            } else {
                console.log("connection establised to ", url);
                var collection = db.collection('structures');
                collection.insertOne({
                    "name": name,
                    "title": title,
                    "author": req.user.username,
                    "category": category,
                    "content": content,
                    "tags": tags,
                    "credit": credit,
                    "screenshot": screenshot,
                    "structure": structure,
                    "votes": count,
                    "voters": [],
                    "created": Date.now()

                });
                req.session.message = "Success! Structure has been submitted";
                res.redirect('/account/dashboard');
            }
        })

    });

    router.post('/submit/profile', isAuthenticated, function (req, res, next) {
        console.log(req.body);
        var twitter = req.body.twitter;
        var youtube = req.body.youtube;
        var website = req.body.website;
        var bio = req.body.content;


        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log(err);
            } else {
                console.log("Successfully connected to ", url);

                db.collection('users').update(
                    {username: req.session.user.username},
                    {
                        $set: {
                            twitter: twitter,
                            youtube: youtube,
                            website: website,
                            bio: bio
                        }
                    }, function (err, result) {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log(result);
                            res.redirect('/account/dashboard');
                        }
                    }
                )
            }
        });
    });

    router.post('/submit/avatar', isAuthenticated, upload.single('avatar'), function (req, res, next) {
        var avatar = req.file['avatar'].filename;
        console.log(avatar);

        res.redirect('/account/dashboard/edit');
    });

    router.post('/submit/settings', isAuthenticated, function (req, res, next) {
        var currentPassword = req.body.currentPassword;
        var newPassword = req.body.newPassword;
        var newPasswordConfirm = req.body.newPasswordConfirm;

    });

    router.get('/edit/structure/:name', isAuthenticated, (function (req, res, next) {
        var structureName = req.params.name;
        var query = {};
        query["name"] = structureName;

        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log(err);
            } else {
                console.log("connection established to ", url);

                var collection = db.collection('structures');

                collection.findOne(query, function (err, doc) {
                    if (err) {
                        console.log(doc);
                    } else if (doc) {
                        var structureAuthor = doc['author'];

                        if (structureAuthor == req.session.user.username) {
                            res.render('editor', {
                                layout: 'sidebarless',
                                name: doc['name'],
                                title: doc['title'],
                                author: doc['author'],
                                category: doc['category'],
                                content: doc['content'],
                                tags: doc['tags'],
                                credit: doc['credit'],
                                screenshot: doc['screenshot'],
                                structure: doc['structure'],
                                username: req.session.user.username
                            });
                        } else {
                            res.send("BAD PERSON");
                        }


                    } else {

                    }
                });
            }
        })

    }));


    /* Handle Logout */
    router.get('/signout', function (req, res) {
        req.session.destroy();
        req.logout();
        res.redirect('/');
    });

    function getUsername(req) {
        var user = req.session.user;
        var username;
        if (user) {
            return user.username;
        } else {
            return null;
        }
    }

    function getUserid(req) {
        var user = req.session.user;
        if (user) {// if there is a user
            return user._id;
        } else {
            return null;
        }
    }

    return router;
}
